# Pruebas a una función escrita en FunC para calcular máximo divisor común entre 2 números

## Prerequisitos

Contar con las herramientas básicas de TON.

Instrucciones para esto con adJ disponibles en
<http://pasosdejesus.github.io/usuario_adJ/conf-programas.html#ton>

## Ejecución de pruebas

	toncli run_tests

o más simple

	make

## Estructura del proyecto

```
.
├── build
│   ├── contract.fif - compilado de of func/code.fc
│   └──contract_tests.fif - compilado automaticamente de tests/example.fc
├── func
│   └── code.fc - Código con la función por probar
├── project.yaml - Estructura del proyecto para toncli
├── README.md
└── tests
    └── example.fc - Código de prueba
```

P.S. Si realmente tiene interes sobre como funcionan las pruebas -- revisar
[aquí](https://github.com/disintar/toncli/blob/master/docs/advanced/func_tests.md)
